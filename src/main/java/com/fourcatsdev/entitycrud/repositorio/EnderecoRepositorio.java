package com.fourcatsdev.entitycrud.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.entitycrud.modelo.Endereco;

public interface EnderecoRepositorio extends JpaRepository<Endereco, Long> {

}
